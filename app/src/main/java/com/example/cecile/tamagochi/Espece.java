package com.example.cecile.tamagochi;

import android.database.sqlite.SQLiteDatabase;

import java.util.Random;

/**
 * Created by cecile on 09/05/2017.
 */
public class Espece {
    int idEspece;
    String nomEspece;
    String dirImage;

    public Espece(int idEspece, String nomEspece, String dirImage){
        this.dirImage = dirImage;
        this.idEspece = idEspece;
        this.nomEspece = nomEspece;
    }

    public int getIdEspece(){
        return this.idEspece;
    }
    public static Espece choisiEspece(SQLiteDatabase db) {
        Random r = new Random();
        int nbEspece = BaseDeDonneesHelper.getNbEspece(db);
        int idEspece = r.nextInt(nbEspece);
        Espece espece = BaseDeDonneesHelper.getEspeceFromId(idEspece, db);
        return espece;
        //return new Espece(1, "Pingouin", "");
    }
}
