package com.example.cecile.tamagochi;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.io.File;

/**
 * Created by ensai on 09/05/17.
 */

public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);

 //       if (doesDatabaseExist(getBaseContext())){
  //          Toast toast = Toast.makeText(getBaseContext(), "Database already created", Toast.LENGTH_SHORT);
 //           toast.show();
//        }
//        else{
//            Toast toast = Toast.makeText(getBaseContext(), "No data for now", Toast.LENGTH_SHORT);
 //           toast.show();
 //           BaseDeDonneesHelper bdd = new BaseDeDonneesHelper(getBaseContext());
 //           SQLiteDatabase readableDataBase = bdd.getReadableDatabase();
 //       }
    }

    private static boolean doesDatabaseExist(Context context) {
        File dbFile = context.getDatabasePath("tamagochi");
        return dbFile.exists();
    }
}