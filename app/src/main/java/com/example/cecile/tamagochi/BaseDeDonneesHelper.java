package com.example.cecile.tamagochi;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

/**
 * Created by cecile on 09/05/2017.
 */
public class BaseDeDonneesHelper extends SQLiteOpenHelper{
    public static final String DBNAME = "tamagochi";
    public  static final int DBVERSION = 1;
    private static final String TAG = "BaseDeDonneesHelper";
    public BaseDeDonneesHelper(Context context){
        super(context, DBNAME, null, DBVERSION);
    }
    public SQLiteDatabase db;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE travail(idTravail INTEGER PRIMARY KEY AUTOINCREMENT, nomTravail TEXT, salaire NUMERIC, debutHoraire INTEGER, finHoraire INTEGER, nivEdMoyen INTEGER)");
        db.execSQL("INSERT INTO travail VALUES(0, 'Ouvrier', 11., 7, 19, 2)");
        db.execSQL("INSERT INTO travail VALUES(1, 'Employé', 10.6, 9, 18, 2)");
        db.execSQL("INSERT INTO travail VALUES(2, 'Cadre Supérieur', 25.5, 9, 18, 2)");
        db.execSQL("INSERT INTO travail VALUES(3, 'Profession intermédiaire', 14.6, 10, 18, 2)");
        db.execSQL("INSERT INTO travail VALUES(4, 'Chômage', 6.4, 0, 0, 0)");
        //db.execSQL("INSERT INTO travail VALUES(2, 'Politicien', 60000, 12, 21, 2)");
        //db.execSQL("INSERT INTO travail VALUES(3, 'Statisticien', 3000, 9, 18, 5)");


        db.execSQL("CREATE TABLE espece(idEspece INTEGER, nomEspece TEXT, dirImage TEXT)");
        db.execSQL("INSERT INTO espece VALUES(0, 'Chat', '')");
        db.execSQL("INSERT INTO espece VALUES(1, 'Pingouin', '')");
        db.execSQL("INSERT INTO espece VALUES(2, 'Homard', '')");

        db.execSQL("CREATE TABLE personnage(idPerso INTEGER PRIMARY KEY AUTOINCREMENT, nomPerso TEXT, argent NUMERIC, moral INTEGER, energy INTEGER, faim INTEGER, idTravail INTEGER, idEspece INTEGER, CONSTRAINT fk_personnagetravail FOREIGN KEY (idTravail) REFERENCES travail(idTravail), CONSTRAINT fk_personnageespece FOREIGN KEY (idEspece) REFERENCES espece(idEspece))");
        //db.execSQL("INSERT INTO personnage VALUES(1, 'HomarMichel',  200000, 10, 0, 3, 2 )");
        //db.execSQL("INSERT INTO personnage VALUES(2, 'AnneDB', 100000000, 10, 0, 2, 1 )");
        db.execSQL("CREATE TABLE actionPossible(idAction INTEGER PRIMARY KEY, nomAction TEXT, duree NUMERIC)");
        db.execSQL("INSERT INTO actionPossible VALUES(0, 'Manger', 1800)");
        db.execSQL("INSERT INTO actionPossible VALUES(1, 'Dormir', 21600)");
        db.execSQL("INSERT INTO actionPossible VALUES(2, 'Jouer', 3600)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static int enterPersoInDB(SQLiteDatabase db, Personnage perso) {
        SQLiteStatement stmt = db.compileStatement("INSERT INTO personnage VALUES(null, ?, ?, ?, ?, ?, ?, ? )");
        stmt.bindString(1, perso.getNom());
        stmt.bindDouble(2, perso.getArgent());
        stmt.bindLong(3, perso.getMoral());
        stmt.bindLong(4, perso.getEnergy());
        stmt.bindLong(5, perso.getFaim());
        stmt.bindLong(6, perso.getTravail().getIdTravail());
        stmt.bindLong(7, perso.getEspece().getIdEspece());
        stmt.executeInsert();
        stmt.close();
        Cursor res=db.rawQuery("SELECT * FROM personnage WHERE nomPerso = '"+ perso.getNom() +"' ", null);
        res.moveToFirst();
        return res.getInt(0);
    }


    public static int getNbTravail(SQLiteDatabase db) {
        Cursor res = db.rawQuery("SELECT count(*) FROM travail", null);
        res.moveToFirst();
        return res.getInt(0);
    }

    public static Travail getTravailFromId(int idTravail, SQLiteDatabase db) {
        Cursor res=db.rawQuery("SELECT * FROM travail WHERE idTravail = '"+ idTravail +"' ", null);
        res.moveToFirst();
        Travail travail = new Travail(res.getInt(0), res.getString(1), res.getInt(2), res.getInt(3),res.getInt(4), res.getInt(5));
        return travail;
    }

    public static int getNbEspece(SQLiteDatabase db) {
        Cursor res = db.rawQuery("SELECT count(*) FROM espece", null);
        res.moveToFirst();
        return res.getInt(0);
    }

    public static Espece getEspeceFromId(int idEspece, SQLiteDatabase db) {
        Cursor res=db.rawQuery("SELECT * FROM espece WHERE idEspece = '"+ idEspece +"' ", null);
        res.moveToFirst();
        Espece espece = new Espece(res.getInt(0),res.getString(1),res.getString(2));
        return espece;
    }

    public static Personnage recupPerso(int id, SQLiteDatabase db) {
        Personnage personnage;
        if (id == -1){
            Cursor res = db.rawQuery("SELECT * FROM personnage", null);
            res.moveToFirst();
            personnage = new Personnage(res.getInt(0), res.getString(1),res.getDouble(2), res.getInt(3),res.getInt(4), res.getInt(5), getTravailFromId(res.getInt(6), db), getEspeceFromId(res.getInt(7), db));
        }
        else{
            Cursor res = db.rawQuery("SELECT * FROM personnage WHERE idPerso = '" + id + "'", null);
            res.moveToFirst();
            personnage = new Personnage(res.getInt(0), res.getString(1),res.getDouble(2), res.getInt(3),res.getInt(4), res.getInt(5), getTravailFromId(res.getInt(6), db), getEspeceFromId(res.getInt(7), db));
        }
        return personnage;
    }

    public static boolean isEmpty(SQLiteDatabase db){
        return db == null;
    }

}
