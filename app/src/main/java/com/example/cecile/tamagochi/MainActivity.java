package com.example.cecile.tamagochi;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.cecile.tamagochi.MESSAGE";
    SQLiteDatabase db;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = this;
        db = openOrCreateDatabase("tamagochi", SQLiteDatabase.CREATE_IF_NECESSARY, null);
       // if (BaseDeDonneesHelper.isEmpty(db)){
        //    Toast.makeText(mContext, "No data for now", Toast.LENGTH_SHORT);
        //}
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button mButton = (Button) findViewById(R.id.bt_create_pers);

        mButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.choix_nom);
                String s = (String) editText.getText().toString();
                Personnage nouvPerso = new Personnage(s, db);
                nouvPerso.setId(BaseDeDonneesHelper.enterPersoInDB(db, nouvPerso));
                mButton.setVisibility(View.GONE);
                editText.setVisibility(View.GONE);
                //launch view of tamagochi
                Intent intent = new Intent(mContext, TamagoDisplayActivity.class);
                String message = String.valueOf(nouvPerso.getId());
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);

            }
        });



    }
}
