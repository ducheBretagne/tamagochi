package com.example.cecile.tamagochi;

import android.database.sqlite.SQLiteDatabase;

import java.util.Random;

import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

/**
 * Created by cecile on 09/05/2017.
 */
public class Personnage {
    int id;
    String nom;
    double argent;
    int moral;
    int energy;
    int faim;
    Travail travail;
    Espece espece;

    public Personnage(String nom, SQLiteDatabase db) {
        Random r = new Random();
        this.nom = nom;
        //this.argent = Outils.loiNormale(1000., 1.);
        this.argent = r.nextGaussian()*1000 +1000;
        this.moral = 8;
        this.energy = 8;
        this.faim = 8;
        this.travail = Travail.choisiTravail(db);
        this.espece = Espece.choisiEspece(db);
    }

    public Personnage(int id, String nom, double argent, int moral, int energy, int faim, Travail travail, Espece espece) {
        this.id = id;
        this.nom = nom;
        this.argent = argent;
        this.moral = moral;
        this.energy = energy;
        this.faim = faim;
        this.travail = travail;
        this.espece = espece;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getArgent() {
        return argent;
    }

    public void setArgent(double argent) {
        this.argent = argent;
    }

    public int getMoral() {
        return moral;
    }

    public int getFaim(){
        return faim;
    }

    public void setFaim(int faim){
        this.faim = faim;
    }

    public void setMoral(int moral) {
        this.moral = moral;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public Travail getTravail() {
        return travail;
    }

    public void setTravail(Travail travail) {
        this.travail = travail;
    }

    public Espece getEspece() {
        return espece;
    }

    public void setEspece(Espece espece) {
        this.espece = espece;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



}
