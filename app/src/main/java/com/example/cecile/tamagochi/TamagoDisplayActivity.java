package com.example.cecile.tamagochi;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * Created by cecile on 25/05/2017.
 */
public class TamagoDisplayActivity extends AppCompatActivity {

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tamalayout);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        SQLiteDatabase db = openOrCreateDatabase("tamagochi", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        Personnage perso = BaseDeDonneesHelper.recupPerso(Integer.parseInt(message), db);

        // Capture the layout's TextView and set the string as its text
        TextView textView = (TextView) findViewById(R.id.tamaname);
        textView.setText(perso.getNom().toUpperCase());

        ImageView mEspeceImage = (ImageView) findViewById(R.id.imageEspece);
        mEspeceImage.setImageResource(R.drawable.babytux);
        ProgressBar mPbEnergy = (ProgressBar) findViewById(R.id.progressBarEnergy);
        mPbEnergy.setMax(10);
        mPbEnergy.setProgress(perso.getEnergy());

        ProgressBar mPbMoral = (ProgressBar) findViewById(R.id.progressBarMoral);
        mPbMoral.setMax(10);
        mPbMoral.setProgress(perso.getMoral());

        ProgressBar mPbFaim = (ProgressBar) findViewById(R.id.progressBarFaim);
        mPbFaim.setMax(10);
        mPbFaim.setProgress(perso.getFaim());

        TextView capital = (TextView) findViewById(R.id.tam_capital);
        DecimalFormat df = new DecimalFormat("#.##");
        capital.setText("Capital : " + df.format(perso.getArgent()));

        TextView metier = (TextView) findViewById(R.id.tam_metier);
        metier.setText("Métier : " + perso.getTravail().getNom());

        TextView salaire = (TextView) findViewById(R.id.tam_salaire);
        salaire.setText("Salaire : " + perso.getTravail().getSalaire());

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.menu_title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    private void addDrawerItems() {
        String s0 = getString(R.string.menu_item_liste_tamagochi);
        String s1 = getString(R.string.menu_item_eat);
        String s2 = getString(R.string.menu_item_sleep);
        String s3 = getString(R.string.menu_item_friends);
        String[] osArray = {s0,s1,s2,s3};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
    }
}
