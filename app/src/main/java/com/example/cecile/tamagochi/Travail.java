package com.example.cecile.tamagochi;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

import java.util.Date;
import java.util.Random;

/**
 * Created by cecile on 09/05/2017.
 */
public class Travail {
    int id;
    int salaire;
    String nom;
    int debutHoraire;
    int finHoraire;
    int nivEdMin;
    //on pourrait ajouter un salaire femme

    public Travail(int id, String nom, int salaire, int debutHoraire, int finHoraire, int nivEdMin){
        this.id = id;
        this.salaire = salaire;
        this.nom = nom;
        this.debutHoraire = debutHoraire;
        this.finHoraire = finHoraire;
        this.nivEdMin = nivEdMin; /*niveau de diplome*/
    }

    public static Travail choisiTravail(SQLiteDatabase db) {
        Random r = new Random();
        int nbTravail = BaseDeDonneesHelper.getNbTravail(db);
        int idTravail = r.nextInt(nbTravail); //à enlever dès que base relancée;
        Travail travail = BaseDeDonneesHelper.getTravailFromId(idTravail, db);
        return travail;
        //return new Travail(0, "Chomeur", 0, 0, 0);
    }

    public int getIdTravail(){
        return this.id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDebutHoraire() {
        return debutHoraire;
    }

    public void setDebutHoraire(int debutHoraire) {
        this.debutHoraire = debutHoraire;
    }

    public int getFinHoraire() {
        return finHoraire;
    }

    public void setFinHoraire(int finHoraire) {
        this.finHoraire = finHoraire;
    }

    public int getNivEdMin() {
        return nivEdMin;
    }

    public void setNivEdMin(int nivEdMin) {
        this.nivEdMin = nivEdMin;
    }
}
